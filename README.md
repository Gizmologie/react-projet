# <b>Projet blog</b>

## Technologies
- Front : ReactJS
- Back : NodeJS 
- Framework back : ExpressJS

## Equipe 

Benjamin ROBERT <br>
Aurélien GUILLEMOT


## Contexte 

L'objectif de ce blog est de nous aider à préparer notre projet annuel. 
Le thème est : Campus 2030, la vie sur le campus. Nous avons décider pour ce thème de proposer un 
blog qui nous permettra de voir les différents articles proposer par nos utilisateurs. 

## Procedure d'installation 

Back : 
1. npm install / yarn install
2. npm run dev / yarn dev

Front : 
1. npm install / yarn install
2. npm start / yarn start

## Configuration de l'environnement

La configuration peut se faire dans le fichier .env.example

Front :<br>**REACT_APP_HOST_API** = adresse/port du back

Back : <br>**APP_PORT**=<br>**DB_URL**=adresse/port de la BDD
       <br>**JWT_SECRET**=random_string


## Base de données

BDD : MongoDB

## Problèmes rencontrés

Suite à des tests en fin de projet, nous nous sommes aperçu que nous avions oublier les delete en cascade. Nous avons essayer de regler ces problèmes là au plus vite car cela pouvait causer des problèmes sur l'ensemble du site.
    
