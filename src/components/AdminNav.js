import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class AdminNav extends Component{

    render() {

        return <div className="navbar-nav ">
            <Link to={'/admin/users'} className="nav-link">ADMIN | Utilisateurs</Link>
            <Link to={'/admin/categories'} className="nav-link">ADMIN | Catégories</Link>
        </div>
    }
}
