import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {updateUser} from "../actions/users.actions.js";
import {connect} from 'react-redux';
import PublicNav from './PublicNav.js';
import AdminNav from './AdminNav';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

class Header extends Component{

    logout(){
        localStorage.removeItem('tokenAuth');
        this.props.updateUser(null);
        window.history.pushState({}, null, '/')
    }

    render() {
        return <nav className="navbar navbar-expand-lg navbar-dark bg-gradient-primary font-weight-bold">

            <Link to={'/'} className={"navbar-brand"}>Blog</Link>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <PublicNav/>
                    {
                        this.props.user && this.props.user.role === "10" ?
                            <AdminNav/> : ''
                    }

            </div>
            <div className="mr">
                <div className="navbar-nav">
                    {
                        this.props.user ?
                            <div>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {
                                            this.props.user.thumbnail ?
                                                <img src={this.props.user.thumbnail} alt="" className={'img-fuild rounded-circle mr-2'} width={'20px'}/> :
                                                <FontAwesomeIcon icon={faUser} className={"mr-2"}/>
                                        }
                                        Mon compte
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <Link to={'/profile'} className="dropdown-item">Mon compte</Link>
                                        <Link to={'/articles'} className="dropdown-item">Mes articles</Link>
                                        <div className="dropdown-divider"/>
                                        <a onClick={() => this.logout()} className="dropdown-item" href="#">Déconnexion</a>

                                    </div>
                                </li>
                            </div>
                            : <Link to={'/connexion'} className="nav-link">Connexion</Link>
                    }

                </div>
            </div>
        </nav>
    }
}
const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))}
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
