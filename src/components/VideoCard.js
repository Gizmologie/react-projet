import React, {Component} from 'react';

export default class VideoCard extends Component{

    render() {
        let {title, description, picture, genres} = this.props;

        return <div className="card">
            {
                picture && <img className="card-img-top" src={picture} alt="Card image cap"/>
            }
            <div className="card-body">
                <h5 className="card-title">{title}</h5>
                <p className="card-text">{description}</p>
                <div>
                    {
                        genres && genres.map((genre, index) => {
                            return <span key={index} className="mr-2 badge badge-secondary">{genre.libelle}</span>
                        })
                    }
                </div>
                <a href="#" className="btn btn-primary">Voir le film</a>
            </div>
        </div>
    }
}
