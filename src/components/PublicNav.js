import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class PublicNav extends Component{

    render() {

        return <div className="navbar-nav" >
            <Link to={'/categories'} className="nav-link">Categories</Link>
        </div>
    }
}
