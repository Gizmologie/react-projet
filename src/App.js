import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';
import Home from "./views/Home.js";
import Header from "./components/Header.js";
import CategoriesList from "./views/Categories/CategoriesList.js";

import CreatePost from "./views/Posts/CreatePost";
import PostsList from "./views/Posts/PostsList.js";
import PostDetail from "./views/Posts/PostDetail";
import PostByCategory from "./views/Posts/PostByCategory";
import UpdatePost from "./views/Posts/UpdatePost";

import Login from "./views/Profil/Login.js";
import UsersList from "./views/Admin/Users/List";
import AdminCategory from "./views/Admin/Categories/List";
import UserProfil from "./views/Profil/Index";

import {connect} from 'react-redux';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons'
import UpdateComment from "./views/Posts/UpdateComment";
import Register from './views/Profil/Register';

library.add(fab, faCheckSquare, faCoffee)


class App extends Component{


    render() {
        return <BrowserRouter>
            <Header />
            <Route exact path="/" component={Home}/>

            {/*Mes routes*/}
            <Route exact path="/articles" component={PostsList}/>
            <Route exact path="/categories" component={CategoriesList}/>
            <Route exact path="/connexion" component={Login}/>
            <Route exact path="/inscription" component={Register}/>
            <Route exact path="/articles/categorie/:id" component={PostByCategory}/>
            <Route exact path="/article/:id" component={PostDetail}/>
            <Route exact path="/commentaire/modification/:id" component={UpdateComment}/>

            {
                this.props.user ? <div>
                        <Route exact path="/profile" component={UserProfil}/>
                        <Route exact path="/post/create" component={CreatePost}/>
                        <Route exact path="/post/update/:id" component={UpdatePost}/>
                    </div>
                    : ''
            }

            {
                this.props.user && this.props.user.role === "10" ? <div>
                        <Route exact path="/admin/users" component={UsersList}/>
                        <Route exact path="/admin/categories" component={AdminCategory}/>
                    </div>
                    : ''
            }

        </BrowserRouter>
    }
}

const mapStateToProps = state => {
    return {user: state.user}
};

export default connect(mapStateToProps, null)(App);
