import axios from 'axios';
import API from "./Base";

export default class PostsService{

    /**
     * Get post's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/posts`);
    }


    /**
     * Get post's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async last(){
        return await API.get(`${process.env.REACT_APP_HOST_API}/posts/last`).then((res) => {
            return res.data.posts;
        });
    }

    /**
     * Get post's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async byUser(){
        return await API.get(`${process.env.REACT_APP_HOST_API}/posts/user`).then((res) => {
            return res.data.posts;
        });
    }

    /**
     *
     * @param id
     * @returns {Promise<AxiosResponse<any>>}
     */
    static async details(id){
        // return await axios.get(`${process.env.REACT_APP_HOST_API}/posts/${id}`);
        return await API.get(`/posts/${id}`).then((res) => {
            return res.data.post
        });
    }

    /**
     *
     * @param category
     * @returns {Promise<AxiosResponse<any>>}
     */
    static async listByCategory(category){
        return await API.get(`${process.env.REACT_APP_HOST_API}/posts/category/${category}`).then((response) => {
            return response.data.posts;
        });
    }

    /**
     *
     * @param body
     */
    static async store(body){
        return await API.post('/posts', body).then((response) => {
            return response.data.post
        });
    }

    /**
     *
     * @param id
     * @param body
     * @returns {Promise<[]>}
     */
    static async update(id, body){
        return await API.put(`/posts/${id}`, body).then((response) => {
            return response.data.users // à modifier
        });
    }

    /**
     *
     * @param id
     * @returns {Promise<[]>}
     */
    static async delete(id){
        return await API.delete(`/posts/${id}`).then((response) => {
            return response.data.posts
        });
    }
}
