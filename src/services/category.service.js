import axios from 'axios';
import API from "./Base";

export default class CategoryService{

    /**
     * Get genre's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await API.get(`${process.env.REACT_APP_HOST_API}/categories`).then((res) => {
            return res.data.categories
        });
    }

    /**
     * Create one genre
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async create(body){
        return await API.post(`${process.env.REACT_APP_HOST_API}/categories`, body);
    }

    /**
     * Update one genre
     * @param id
     * @param body
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async update(id, body){
        return await API.put(`${process.env.REACT_APP_HOST_API}/categories/${id}`, body).then((response) => {
            return response.data.category
        });
    }

    /**
     * Delete one genre
     * @param id
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async delete(id){
        return await API.delete(`${process.env.REACT_APP_HOST_API}/categories/${id}`);
    }


    static async getCategory(id){
        return await API.get(`/categories/${id}`).then((response) => {
            return response.data.category
        });
    }
}
