import axios from 'axios';
import API from "./Base";

export default class CommentService{

    /**
     * Get genre's list
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async listByPost(id){
        return await API.get(`/comments/post/${id}`).then((response) => {
            return response.data.comments
        });
    }


    /**
     *
     * @param id
     * @returns {Promise<AxiosResponse<any>>}
     */
    static async details(id){
        return await API.get(`/comment/${id}`).then((response) => {
            return response.data.comment
        });
    }


    /**
     *
     * @param category
     * @returns {Promise<AxiosResponse<any>>}
     */
    static async listByCategory(category){
        return await axios.get(`${process.env.REACT_APP_HOST_API}/posts/category/${category}`);
    }


    /**
     *
     * @param body
     * @returns {Promise<{success: boolean}>}
     */
    static async store( body){
        return await API.post(`/comment`, body).then((response) => {
            return {success: true}
        });
    }

    /* /!**
      * Update one genre
      * @param id
      * @param body
      * @returns {Promise<AxiosResponse<T>>}
      *!/
     static async update(id, body){
         return await axios.put(`${process.env.REACT_APP_HOST_API}/genres/${id}`, body);
     }*/

    /**
     *
     * @param id
     * @param body
     * @returns {Promise<[]>}
     */
    static async update(id, body){
        return await API.put(`/comment/${id}`, body).then((response) => {
            return response.data.users
        });
    }

    /**
     *
     * @param id
     * @returns {Promise<[]>}
     */
    static async delete(id){
        return await API.delete(`/comment/${id}`).then((response) => {
            return response.data.posts
        });
    }
}
