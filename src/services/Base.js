import axios from "axios";

export default axios.create({
    baseURL: `${process.env.REACT_APP_HOST_API}`,
    responseType: "json",
    headers: {
        Authorization: `Bearer ${localStorage.getItem('tokenAuth')}`
    }
});
