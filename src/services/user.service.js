import API from "./Base";
import axios from 'axios';

export default class UserService{

    static async auth(body){
        return await API.post('/login', body);
    }

    /**
     * Get users
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async list(){
        return await API.get('/users').then((res) => {
            return res.data.users
        });
    }

    /**
     * Get user
     * @returns {Promise<AxiosResponse<T>>}
     */
    static async account(){
        return await API.get('/profile').then((res) => {
            return res.data
        });
    }

    static async accountCreate(body){
        return await axios.post(`${process.env.REACT_APP_HOST_API}/profile`, body, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            return response.data.user
        });
    }

    static async accountUpdate(id, body){
        return await API.put(`/profile`, body, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            return response.data.user
        });
    }


    static async store(body){
        return await API.post('/users', body, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            return {user: response.data.user}
        });
    }

    static async update(id, body){
        return await API.put(`/users/${id}`, body, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            return response.data.user
        });
    }

    static async delete(id){
        return await API.delete(`/users/${id}`).then((response) => {
            return response.data.users
        });
    }

}
