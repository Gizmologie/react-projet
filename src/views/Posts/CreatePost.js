import React, {Component} from 'react';
import PostsService from "../../services/post.service";
import CategoryService from "../../services/category.service";


export default class CreatePost extends Component {

    constructor(props) {
        super(props);
        this.state = {
            post : {
                title: '',
                content: '',
                category: ''
            },
            categories: []
        };
    }


    async componentDidMount() {
        try{
            let categories = await CategoryService.list();
            this.setState({categories: categories});
        }catch (e) {
            console.error(e);
        }
    }

    async handleChange(e) {
        this.setState({
            post: {
                ...this.state.post,
                [e.target.name]: e.target.value,
            }
        });
    }

    async formSubmit(e) {
        e.preventDefault()
        try{
           let post = await PostsService.store(this.state.post);
           this.props.history.push(`/articles`);
        }catch (e) {
            alert(e)
        }
    }

    render() {
        let {categories}= this.state;
        return <div className="container">
            <h1>Créer un article</h1>
            <div className="col-md-5">
                <div className="form-area">
                    <form id={"PostCreate"} onSubmit={async (event) => await this.formSubmit(event)}>
                        <br/>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Titre"
                                name='title'
                                id={'title'}
                                value={this.state.title}
                                onChange={(e) => this.handleChange(e)}
                                required/>
                        </div>

                        <div className="form-group">
                            <textarea
                                name="content"
                                className="form-control"
                                id={'content'}
                                value={this.state.content}
                                onChange={(e) => this.handleChange(e)}
                                rows="7"/>
                        </div>

                        <div className="form-group">

                            <select
                                className="form-control"
                                name="category" id="category"
                                onChange={(e) => this.handleChange(e)}
                                required="required">
                                <option value=''>Choisir la catégorie</option>
                                {
                                    categories.map((category, index) => {
                                        return <option
                                            value={category.id}
                                            key={index}>
                                            {category.label}
                                        </option>
                                    })
                                }
                            </select>
                        </div>
                        <button type="submit"  className="btn btn-primary pull-right">Publier</button>
                    </form>

                </div>
            </div>
        </div>
    }
}
