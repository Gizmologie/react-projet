import React, {Component} from 'react';
import PostsService from "../../services/post.service";
import PostService from "../../services/post.service";
import CategoryService from "../../services/category.service";


export default class UpdatePost extends Component {

    constructor(props) {
        super(props);
        this.state = {
            post : {
                _id: '',
                title: '',
                content: '',
                category: ''
            },
            categories : []
        };
    }


    async componentDidMount() {
        try{
            let categories = await CategoryService.list();
            let post = await PostService.details(this.props.match.params.id);
            this.setState({
                post: post,
                categories: categories
            });
        }catch (e) {
            console.error(e);
        }
    }

    async handleChange(e) {
        this.setState({
            post: {
                ...this.state.post,
                [e.target.name]: e.target.value,
            }
        });
    }

    async formSubmit(e) {
        e.preventDefault()
        try{
            await PostsService.update(this.state.post._id, this.state.post);
            this.props.history.push('/articles');
        }catch (e) {
            alert(e)
        }
    }

    isCategoryOfPost(post, category) {
        return JSON.stringify(post.category) === JSON.stringify(category.id)
    }

    render() {
        let {categories}= this.state;
        let {post} = this.state;

        return <div className="container">
            <h1>Modifier un article</h1>
            <div className="col-md-5">
                <div className="form-area">
                    <form id={"PostCreate"} onSubmit={async (event) => await this.formSubmit(event)}>
                        <br/>
                        <div className="form-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Titre"
                                name='title'
                                id={'title'}
                                value={post.title}
                                onChange={(e) => this.handleChange(e)}
                                required/>
                        </div>

                        <div className="form-group">
                            <textarea
                                name="content"
                                className="form-control"
                                id={'content'}
                                value={post.content}
                                onChange={(e) => this.handleChange(e)}
                                rows="7"/>
                        </div>

                        <div className="form-group">
                            <select
                                required={"required"}
                                className="form-control"
                                name="category"
                                id="category"
                                onChange={(e) => this.handleChange(e)}>
                                <option value=''>Choisir la catégorie</option>
                                {
                                    categories.map((category, index) => {
                                        return <option
                                            value={category.id}
                                            selected={this.isCategoryOfPost(
                                                post, category)
                                                ? 'selected'
                                                : ''}
                                            key={index}>
                                            {category.label}
                                        </option>
                                    })
                                }
                            </select>
                        </div>
                        <button type="submit"  className="btn btn-primary pull-right">Modifier</button>
                    </form>

                </div>
            </div>
        </div>
    }
}
