import React, {Component} from 'react';
import PostService from '../../services/post.service';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import CommentService from "../../services/comment.service";

export default class PostsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            posts: []
        };
    }

    async componentDidMount() {
        this.setState({
            posts: await PostService.byUser()
        });
    }

    async deletePost(id) {
        let listComment = await CommentService.listByPost(id);
        listComment.map(async (comment) => {
            await CommentService.delete(comment._id);
        })
        await PostService.delete(id);
        await this.setState({
            posts: await PostService.byUser()
        });
    }

    render() {
        let {posts} = this.state;
        return <div className="container-fluid mt-5">

            <div className="row">
                <div className="col-12 mb-3">
                    <h1 className="h3 mb-0 text-gray-800">Vos articles</h1>
                    <p>
                        Vous pouvez gérer les articles que vous avez créé.
                    </p>
                </div>

                <div className="col-12">
                    <div className="card shadow mb-4 border-left-success">
                        <div className="card-header py-3">
                            <h6 className="text-primary text-uppercase">Vos articles</h6>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Date</th>
                                            <th>Catégorie</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            posts.map((post, index) => {
                                                return <tr key={index}>
                                                    <td>{post.title}</td>
                                                    <td>{moment(post.created_at).format('DD/MM/Y')}</td>
                                                    <td>{post.category.label}</td>

                                                    <td>
                                                        {/* eslint-disable-next-line no-restricted-globals */}
                                                        <button className="btn btn-danger" onClick={() => confirm(
                                                            'Êtes-vous sûr de vouloir supprimer cet article ?')
                                                            ? this.deletePost(
                                                                post._id)
                                                            : null}>
                                                            <FontAwesomeIcon icon={faTrash}/>
                                                        </button>
                                                        <button
                                                            className="btn btn-warning ml-2"
                                                            type="button"
                                                            onClick={(e) => {
                                                                window.location.href = `/post/update/${post._id}`;
                                                            }}>
                                                            <FontAwesomeIcon icon={faEdit}/>
                                                        </button>
                                                    </td>
                                                </tr>;
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-12">

                    <a className="btn btn-success btn-icon-split" href="/post/create" role={"button"}>
                    <span className="icon text-white-50">
                        <FontAwesomeIcon icon={faPlus}/>
                    </span>
                        <span className="text">Ecrire
                        un article</span>
                    </a>
                </div>
            </div>

        </div>;
    }
}
