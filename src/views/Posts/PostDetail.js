import React, {Component} from 'react';
import PostService from "../../services/post.service";
import CategoryService from "../../services/category.service";
import CommentService from "../../services/comment.service";
import {updateUser} from "../../actions/users.actions";
import {connect} from "react-redux";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlus, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';



class PostDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            post: {
                user: {
                    firstname: '',
                    lastname: '',
                    email: '',
                    created_at: ''
                }}
            ,
            category: {
                label: ''
            },
            comments: [{
                title: '',
                content: '',
                post: '',
                user: ''
            }],
            add: [{
                title: '',
                content: '',
                post: '',
                user: ''
            }]
        }
    }


    async componentDidMount() {
        try{

            let post = await PostService.details(this.props.match.params.id);
            let category = await CategoryService.getCategory(post.category);
            let comments = await CommentService.listByPost(post._id);
            this.setState({
                post: post,
                category: category,
                comments: comments
           });
        }catch (e) {
            console.error(e);
        }
    }

    async handleChange(e) {
        this.setState({
            add: {
                ...this.state.add,
                [e.target.name]: e.target.value,
            }
        });
    }

    async deleteCom(id) {
        try {
            await CommentService.delete(id);
        }catch (e) {
            console.error(e);
        }
    }

    async formSubmit(e) {
        e.preventDefault()
        try{
            this.state.add.post = this.props.match.params.id;
            await CommentService.store(this.state.add);
            window.location.reload(false)
        }catch (e) {
            alert(e)
        }
    }


    render() {
        let {post} = this.state
        let {category} = this.state
        let {comments} = this.state

        return <div className="row">
            {/*Post*/}
                <div className="col-md-10 offset-md-1 mt-5">
                    <div className="card flex-md-row mb-4 box-shadow h-md-250">
                        <div className="card-body d-flex flex-column align-items-start">
                            <strong className="d-inline-block mb-2 text-primary">{post.title}</strong>
                            <h3 className="mb-0">
                                <a className="text-dark">{category.label}</a>
                            </h3>
                            <div className="mb-1 text-muted">{post.user.firstname} {post.user.lastname}</div>
                            <hr className='solid w-100'/>
                            <p className="card-text mb-auto">{post.content}</p>
                        </div>
                    </div>
                </div>
            {/*Fin post*/}
            {/*  Commentaire  */}
            {
                this.props.user !== null &&
                <div className="col-md-6 offset-md-3"><br/>
                    {/*Création des commentaires*/}
                    <div className="col-md-12">
                        <form id={"CommentCreate"} onSubmit={async (event) => await this.formSubmit(event)}>
                            <br/>
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control input-material w-25"
                                    placeholder="Titre"
                                    name='title'
                                    id={'title'}
                                    onChange={(e) => this.handleChange(e)}
                                    required/>
                            </div>

                            <div className="form-group">
                            <textarea
                                required
                                name="content"
                                placeholder="Commentaire public"
                                className="form-control input-material"
                                id={'content'}
                                onChange={(e) => this.handleChange(e)}
                                rows="1"/>
                            </div>
                            <button type="submit"  className="btn btn-outline-primary ml-auto d-block">Publier</button>
                        </form>
                    </div>

                    {/*Fin de création des commentaires*/}
                </div>
            }

            <div className="col-md-12">
               <div className="container bootstrap snippets bootdey">
                   <div className="row">
                       <div className="col-md-12">
                           <div className="blog-comment">
                               <h3 className="text-success">Commentaires</h3>
                               <hr/>
                               <ul className="comments">
                               {/*Affichage*/}
                               {
                                   comments && comments.map((com, index) => {
                                       return  <li className="clearfix" key={index}>
                                           <img src="https://bootdey.com/img/Content/user_1.jpg" className="avatar" alt=""/>
                                           <div className="post-comments">
                                               <div className="meta">
                                                   <a href="#">
                                                       <div className="d-flex justify-content-between">
                                                           <span className='h5'>
                                                               {com.user.firstname} {com.user.lastname}
                                                           </span>
                                                           {
                                                               (this.props.user !== null && this.props.user.id === com.user.id) &&
                                                               <div>

                                                                   {/* eslint-disable-next-line no-restricted-globals */}
                                                                   <button className="btn btn-danger" onClick={() => confirm(
                                                                       'Êtes-vous sûr de vouloir supprimer ce commentaire ?')
                                                                       ? this.deleteCom(com._id)
                                                                       : null}>
                                                                       <FontAwesomeIcon icon={faTrash}/>
                                                                   </button>
                                                                   <a href={`/commentaire/modification/${com._id}`} className={'btn btn-warning ml-2'}>
                                                                       <FontAwesomeIcon icon={faEdit}/>
                                                                   </a>

                                                               </div>
                                                           }

                                                       </div>
                                                   </a>
                                                   <br/>
                                                   {com.title}
                                               </div>
                                               <p>{com.content}</p>
                                           </div>
                                       </li>
                                   })
                               }
                               {/*  Fin commentaire  */}

                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

        </div>
    }
}

const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))}
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail);

