import React, {Component} from 'react';
import CommentService from "../../services/comment.service";


export default class UpdateComment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comment : {
                _id: '',
                title: '',
                content: '',
                post: ''
            }
        };
    }

    async componentDidMount() {
        try{
            let comment = await CommentService.details(this.props.match.params.id);
            this.setState({
                comment: comment
            });
        }catch (e) {
            console.error(e);
        }
    }

    async handleChange(e) {
        this.setState({
            comment: {
                ...this.state.comment,
                [e.target.name]: e.target.value,
            }
        });
    }

    async formSubmit(e) {
        e.preventDefault()
        try{
            await CommentService.update(this.props.match.params.id, this.state.comment);
            this.props.history.push(`/article/${this.state.comment.post}`);
        }catch (e) {
            alert(e)
        }
    }

    render() {
        let {comment} = this.state;
        return <div className="container">

            <div className="col-md-12">
                <div className="container bootstrap snippets bootdey">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="blog-comment">
                                <h3 className="text-success">Modifier un commentaire</h3>
                                <hr/>
                                <ul className="comments">
                                    <form id={"PostCreate"} onSubmit={async (event) => await this.formSubmit(event)}>
                                    {/*Affichage*/}
                                    <li className="clearfix">
                                        <img src="https://bootdey.com/img/Content/user_1.jpg" className="avatar" alt=""/>
                                        <div className="post-comments">
                                            <div className="meta">
                                                <input
                                                    type="text"
                                                    className="form-control w-50"
                                                    placeholder="Titre"
                                                    name='title'
                                                    id={'title'}
                                                    value={comment.title}
                                                    onChange={(e) => this.handleChange(e)}
                                                    required/>
                                            </div>
                                            <div className="form-group">
                                                <textarea
                                                    name="content"
                                                    className="form-control"
                                                    id={'content'}
                                                    value={comment.content}
                                                    onChange={(e) => this.handleChange(e)}
                                                    rows="1"/>
                                            </div>
                                            <button type="submit"  className="btn btn-primary pull-right mt-2 ml-auto d-block">Modifier</button>
                                        </div>
                                    </li>
                                    </form>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}
