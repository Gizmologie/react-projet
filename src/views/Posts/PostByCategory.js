import React, {Component} from 'react';
import CategoryService from '../../services/category.service';
import moment from 'moment';

export default class PostByCategory extends Component {

    constructor(props) {
        super(props);
        this.state = {
            category: {
                id: null,
                label: null,
                posts: []
            }
        };
    }

    async componentDidMount() {
        await this.setState({
            category: await CategoryService.getCategory(
                this.props.match.params.id)
        });

    }

    render() {
        let {category} = this.state;
        return <div className="container pt-5">
            <div className="row mb-2">
                <div className="col-12 mb-4">
                    <h1 className={'h3 mb-0 text-gray-800'}>Nos articles sur le
                        thème : {category.label}</h1>
                </div>
                {
                    category.posts.map((post) => {
                        return <div className="col-md-6">
                            <div className="card flex-md-row mb-4 box-shadow h-md-250">
                                <div className="card-body d-flex flex-column align-items-start">
                                    <h3 className="d-inline-block mb-2 text-primary">{post.title}</h3>
                                    <div className="mb-1 text-muted">{moment(
                                        post.created_at).
                                        format('d MMM Y')}</div>
                                    <p className="card-text mb-auto">{post.content}</p>
                                    <a href={`/article/${post._id}`}>Consulter</a>
                                </div>

                            </div>
                        </div>;
                    })
                }
            </div>
        </div>;

    }
}
