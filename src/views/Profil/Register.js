import React, {Component} from 'react';
import UserService from '../../services/user.service.js';
import {connect} from 'react-redux';
import {updateUser} from '../../actions/users.actions.js';
import UserModal from '../Admin/Users/Modal';

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {}
        };
    }

    handleChange(e, type = 'default') {

        let value = null;

        switch (type) {
            case 'file':
                value = e.target.files[0];
                break;
            default:
            case 'default':
                value = e.target.value;
                break;
        }

        this.setState({
            user: {
                ...this.state.user,
                [e.target.id]: value
            }
        });
    }

    async formSubmit(e) {
        e.preventDefault();
        let data = new FormData();
        let value = null;
        let key = null;

        Object.entries(this.state.user).forEach((el) => {
            value = el[1];
            key = el[0];
            value = Array.isArray(value) ? value.map((el) => {
                return el._id;
            }) : value;
            data.append(key, value);
        });

        let user = await UserService.accountCreate(data);

        if (user) {
            let response = await UserService.auth({
                email: user.email,
                password: user.password
            });

            let token = response.data.token;
            localStorage.setItem('tokenAuth', token);
            this.props.updateUser(response.data.user);
            this.props.history.push('/');
        }

    }

    render() {
        return <div className="container">
            <div className="row justify-content-center">
                <div className="col-xl-10 col-lg-12 col-md-9">
                    <div className="card o-hidden border-0 shadow-lg my-5">
                        <div className="card-body p-0">
                            <div className="row">
                                <div className="col-lg-6 d-none d-lg-block bg-register-image"/>
                                <div className="col-lg-6">
                                    <div className="p-5">
                                        <div className="text-center">
                                            <h1 className="h4 text-gray-900 mb-4">
                                                Embarquez dans l'aventure</h1>
                                        </div>
                                        <form className="user" onSubmit={(e) => this.formSubmit(
                                            e)}>
                                            <div className="form-group">
                                                <label>Prénom</label>
                                                <input
                                                    required="required"
                                                    type="text"
                                                    className="form-control"
                                                    id="firstname"
                                                    name={'firstname'}
                                                    value={this.state.user.firstname} onChange={(e) => this.handleChange(
                                                    e)}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Nom</label>
                                                <input
                                                    required="required"
                                                    type="text"
                                                    className="form-control"
                                                    id="lastname"
                                                    name={'lastname'}
                                                    value={this.state.user.lastname} onChange={(e) => this.handleChange(
                                                    e)}/>
                                            </div>
                                            <div className="form-group">
                                                <label>E-mail</label>
                                                <input
                                                    required="required"
                                                    type="email"
                                                    className="form-control"
                                                    id="email"
                                                    name={'email'}
                                                    value={this.state.user.email} onChange={(e) => this.handleChange(
                                                    e)}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Photo de profil</label>
                                                <input type="file"
                                                       className="form-control"
                                                       id="thumbnail"
                                                       name={'thumbnail'}
                                                       onChange={(e) => this.handleChange(
                                                           e, 'file')}/>
                                            </div>
                                            <div className="form-group">
                                                <label>Mot de passe</label>
                                                <input
                                                    required="required"
                                                    type="password"
                                                    className="form-control"
                                                    id="password"
                                                    name={'password'}
                                                    value={this.state.user.password}
                                                    onChange={(e) => this.handleChange(
                                                        e)}/>
                                            </div>
                                            <button type="submit" className="btn btn-primary btn-user btn-block">Inscription</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>;
    }
}

const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))};
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
