import React, {Component} from 'react';
import {connect} from 'react-redux';
import {updateUser} from '../../actions/users.actions.js';
import UserModal from '../Admin/Users/Modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faEdit} from '@fortawesome/free-solid-svg-icons';
import UserService from '../../services/user.service';

class Index extends Component {

    async componentDidMount() {
        document.addEventListener('userModalClose', async (e) => {
            // alert('Les modification seront prises en compte durant votre prochaine connexion')
            this.props.updateUser(await UserService.account());
            this.props.history.push('/profile');
        });
    }

    render() {
        let user = this.props.user;
        return <div className="container-fluid pt-5">

            <div className="row">
                <div className="col-lg-7 col-12 border-right text-center">
                    {
                        user
                            ?
                            <img src={user.thumbnail} alt={user.email} className={'img-fluid'} style={{maxHeight: '500px'}}/>
                            : ''
                    }
                </div>
                <div className="col-lg-5 col-12" >
                    <h2 className={"mb-5"}>
                        Bonjour {user.firstname} {user.lastname}
                    </h2>
                    <p>
                        <strong>Email :</strong> {user.email}
                    </p>
                    <button className="btn btn-success btn-icon-split" data-toggle="modal" data-target="#userModal">
                    <span className="icon text-white-50">
                        <FontAwesomeIcon icon={faEdit}/>
                    </span>
                        <span className="text">Modifier mon profil</span>
                    </button>
                </div>
            </div>

            <UserModal
                id={this.props.user.id}
                firstname={this.props.user.firstname}
                lastname={this.props.user.lastname}
                email={this.props.user.email}
                restricted={true}
            />
        </div>;
    }
}

const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))};
};

export default connect(mapStateToProps, mapDispatchToProps)(Index);
