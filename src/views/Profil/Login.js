import React, {Component} from 'react';
import UserService from '../../services/user.service.js';
import {connect} from 'react-redux';
import {updateUser} from '../../actions/users.actions.js';
import {Link} from 'react-router-dom';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            isError: false
        };
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e) {
        e.preventDefault();
        this.setState({isError: false});
        let {email, password} = this.state;
        let body = {email, password};

        try {
            let response = await UserService.auth(body);
            let token = response.data.token;
            localStorage.setItem('tokenAuth', token);
            this.props.updateUser(response.data.user);
            this.props.history.push('/');
        } catch (e) {
            this.setState({isError: true});
        }
    }

    render() {
        let {isError} = this.state;
        return <div className="container">
            <div className="row justify-content-center">
                <div className="col-xl-10 col-lg-12 col-md-9">
                    <div className="card o-hidden border-0 shadow-lg my-5">
                        <div className="card-body p-0">
                            <div className="row">
                                <div className="col-lg-6 d-none d-lg-block bg-login-image"></div>
                                <div className="col-lg-6">
                                    <div className="p-5">
                                        <div className="text-center">
                                            <h1 className="h4 text-gray-900 mb-4">De
                                                retour chez nous !</h1>
                                        </div>
                                        <form className="user" onSubmit={(e) => this.submit(
                                            e)}>
                                            <div className="form-group">
                                                <input type="email" required className="form-control form-control-user" id="email"
                                                       placeholder="Votre email"
                                                       onChange={(e) => this.handleChange(
                                                           e)}/>
                                            </div>

                                            <div className="form-group">
                                                <input type="password" required className="form-control form-control-user" id="password"
                                                       placeholder={'Votre mot de passe'}
                                                       onChange={(e) => this.handleChange(
                                                           e)}/>
                                            </div>
                                            <button type="submit" className="btn btn-primary btn-user btn-block">Connexion</button>
                                            {isError &&
                                            <p>Erreur d'email et / ou de mot
                                                de passe</p>}

                                        </form>
                                        <hr/>
                                        <div className="text-center">
                                            <Link className="small" to={'/inscription'}>
                                                Vous n'avez pas de compte ?
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>;
    }
}

const mapStateToProps = state => {
    return {user: state.user};
};

const mapDispatchToProps = dispatch => {
    return {updateUser: user => dispatch(updateUser(user))};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
