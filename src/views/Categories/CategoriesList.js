import React, {Component} from 'react';
import CategoryService from '../../services/category.service';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowRight, faUser} from '@fortawesome/free-solid-svg-icons';

export default class CategoriesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: []
        };
    }

    async componentDidMount() {
        try {
            this.setState({categories: await CategoryService.list()});
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let {categories} = this.state;
        return <div className="container-fluid pt-5">
            <div className="row">
                <div className="col-12 mx-xl-5 px-5">
                    <div className="row">
                        <div className="col-12">
                            <div className="d-sm-flex align-items-center justify-content-between mb-4">
                                <h1 className="h3 mb-0 text-gray-800">Nos
                                    thèmes</h1>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {
                            categories.map((category, index) => {
                                return <div key={index} className="col-xl-3 col-md-6 mb-4">
                                    <div className="card border-bottom-primary shadow h-100 pb-2">
                                        <img src={`https://picsum.photos/${(index +
                                            1) * 500}/${((index + 1) * 500) /
                                        2}`} alt="" className={'card-img-top'}/>
                                        <div className="card-body">
                                            <div className="row no-gutters align-items-center">
                                                <div className="col mr-2">
                                                    <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        {category.label}
                                                    </div>
                                                    <div className="h5 mb-0 font-weight-bold text-gray-800">{category.posts.length} articles</div>
                                                    {
                                                        category.posts.length > 0
                                                            ?
                                                            <Link to={`/articles/categorie/${category.id}`} className="btn btn-success mt-3 btn-sm btn-icon-split">
                                                                <span className="icon text-white-50">
                                                                    <FontAwesomeIcon icon={faArrowRight}/>
                                                                </span>
                                                                <span className="text">Consulter</span>
                                                            </Link> : ''
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>;
                            })
                        }

                    </div>

                </div>
            </div>
        </div>;
    }
}
