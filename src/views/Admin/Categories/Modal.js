import React, {Component} from 'react';
import CategoryService from '../../../services/category.service';

export default class Modal extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            category: {
                id: props.id,
                label: props.label,
            },
            editedField: [],
        };
    }

    async componentWillReceiveProps(nextProps) {
        await this.setState({
            category: {
                id: nextProps.id,
                label: nextProps.label,
            },
        });
    }

    async formSubmit(e) {
        e.preventDefault();
        try {
            if (this.state.category.id) {
                await CategoryService.update(this.state.category.id, {
                    label: this.state.category.label
                });
            } else {
                await CategoryService.create({
                    label: this.state.category.label
                });
            }

            document.dispatchEvent(new Event('categoryModalClose'));
            document.querySelector('#categoryModalClose').click();
        } catch (e) {
            alert(e);
        }
    }

    async handleChange(e) {

        this.setState({
            category: {
                ...this.state.category,
                [e.target.name]: e.target.value
            }
        });

        if (this.state.category.id) {
            this.setState({
                editedField: {
                    ...this.state.editedField,
                    [e.target.name]: true
                }
            });

        }
    }


    render() {
        return <div className="modal fade" id="categoryModal" tabIndex="-1" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Catégorie</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" id={'categoryModalClose'}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form id={'addGenderForm'} onSubmit={async (event) => await this.formSubmit(
                            event)}>
                            <div className="form-group">
                                <label>Titre</label>
                                <input required="required" type="text" className="form-control" id="label" name={'label'} value={this.state.category.label} onChange={(e) => this.handleChange(e)}/>
                            </div>

                            <input type="hidden" className="form-control" id="_id" name={'id'} value={this.state.category.id}/>
                            <button type="submit" className="btn btn-primary">{this.state.category.id
                                ? 'Modifier'
                                : 'Ajouter'}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>;
    }
}

