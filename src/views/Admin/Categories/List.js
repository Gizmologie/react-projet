import React, {Component} from 'react';
import CategoryService from '../../../services/category.service.js';
import CategoryModal from './Modal.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlus, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';
import CommentService from "../../../services/comment.service";
import PostService from "../../../services/post.service";

export default class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            category: this.formatCategory()
        };
    }

    async componentDidMount() {
        await this.updateCategories();

        document.addEventListener('categoryModalClose', async (e) => {
            await this.updateCategories();
            await this.resetModal();
        });
    }

    async deleteCategory(id) {
        let articleCategory = await  CategoryService.getCategory(id)
        articleCategory.posts.map(async (post) => {
            let listComment = await CommentService.listByPost(post._id);
            listComment.map(async (comment) => {
                await CommentService.delete(comment._id);
            })
            await PostService.delete(post._id);
        })
        await CategoryService.delete(id);
        await this.updateCategories();
    }

    async updateCategories() {
        this.setState({categories: await CategoryService.list()});
    }

    async resetModal(category = null) {
        category = category ? category : this.formatCategory();

        await this.setState({
            category: category
        });
    }

    formatCategory(category = null) {
        if (category) {
            return {
                id: category.id,
                label: category.label,
            };
        } else {

            return {
                id: '',
                label: ''
            };
        }
    }

    render() {
        let {categories} = this.state;
        return <div className="container-fluid mt-5">

            <div className="row">
                <div className="col-12 mb-3">
                    <h1 className="h3 mb-0 text-gray-800">Catégories</h1>
                    <p>
                        Liste des thèmes disponibles sur le site
                    </p>
                </div>

                <div className="col-12">
                    <div className="card shadow mb-4 border-left-success">
                        <div className="card-header py-3">
                            <h6 className="text-primary text-uppercase">Liste des
                                catégories</h6>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Label</th>
                                            <th>Articles</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            categories.map((category, index) => {
                                                return <tr key={index}>
                                                    <td>{category.label}</td>
                                                    <td>{category.posts.length} articles(s)</td>
                                                    <td>
                                                        {/* eslint-disable-next-line no-restricted-globals */}
                                                        <button className="btn btn-danger" onClick={() => confirm(
                                                            'Êtes-vous sûr de vouloir supprimer cette catégorie ?')
                                                            ? this.deleteCategory(
                                                                category.id)
                                                            : null}>
                                                            <FontAwesomeIcon icon={faTrash}/>
                                                        </button>
                                                        <button onClick={async (e) => this.resetModal(this.formatCategory(category))}
                                                                data-toggle="modal"
                                                                data-target="#categoryModal"
                                                                className={'btn btn-warning ml-2'}>
                                                            <FontAwesomeIcon icon={faEdit}/>
                                                        </button>
                                                    </td>
                                                </tr>;
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-12">
                    <button className="btn btn-success btn-icon-split" onClick={async (e) => this.resetModal()} data-toggle="modal" data-target="#categoryModal">
                    <span className="icon text-white-50">
                        <FontAwesomeIcon icon={faPlus}/>
                    </span>
                        <span className="text">Ajouter une catégorie</span>
                    </button>
                </div>
            </div>


            <CategoryModal
                id={this.state.category.id}
                label={this.state.category.label}
            />

        </div>;
    }
}
