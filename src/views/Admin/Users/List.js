import React, {Component} from 'react';
import UserService from '../../../services/user.service.js';
import UserModal from './Modal.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPlus, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';

export default class List extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            user: this.formatUser()
        };
    }

    async componentDidMount() {
        await this.updateUsers();

        document.addEventListener('userModalClose', async (e) => {
            await this.updateUsers();
            await this.resetModal();
        });
    }

    async deleteUser(id) {
        // await UserService.delete(id);

        await this.updateUsers();
    }

    async updateUsers() {
        this.setState({users: await UserService.list()});
    }

    async resetModal(user = null) {
        user = user ? user : this.formatUser();

        await this.setState({
            user: user
        });
    }

    formatUser(user = null) {
        if (user) {
            return {
                id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                password: null,
                role: user.role
            };
        } else {

            return {
                id: '',
                firstname: '',
                lastname: '',
                email: '',
                password: '',
                role: ''
            };
        }
    }

    render() {
        let {users} = this.state;
        return <div className="container-fluid mt-5">

            <div className="row">
                <div className="col-12 mb-3">
                    <h1 className="h3 mb-0 text-gray-800">Utilisateurs</h1>
                    <p>
                        Liste des utilisateurs ayant un comtpe sur le site (vous ne
                        voyez pas le vôtre)
                    </p>
                </div>

                <div className="col-12">
                    <div className="card shadow mb-4 border-left-success">
                        <div className="card-header py-3">
                            <h6 className="text-primary text-uppercase">Liste des
                                utilisateurs</h6>
                        </div>
                        <div className="card-body">
                            <div className="table-responsive">
                                <table className="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Photo</th>
                                            <th>Nom</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            users.map((user, index) => {
                                                return <tr key={index}>
                                                    <td>
                                                        {
                                                            user.thumbnail ?
                                                                <img src={user.thumbnail} width={'50px'} alt=""/> :
                                                                '-'
                                                        }
                                                    </td>
                                                    <td>{user.firstname} {user.lastname}</td>
                                                    <td>{user.email}</td>
                                                    <td>{user.role}</td>
                                                    <td>
                                                        {/* eslint-disable-next-line no-restricted-globals */}
                                                        <button className="btn btn-danger" onClick={() => confirm(
                                                            'Êtes-vous sûr de vouloir supprimer cet utilisateur ?')
                                                            ? this.deleteUser(
                                                                user._id)
                                                            : null}>
                                                            <FontAwesomeIcon icon={faTrash}/>
                                                        </button>
                                                        <button onClick={async (e) => this.resetModal(
                                                            this.formatUser(
                                                                user))} data-toggle="modal" data-target="#userModal" className={'btn btn-warning ml-2'}>
                                                            <FontAwesomeIcon icon={faEdit}/>
                                                        </button>
                                                    </td>
                                                </tr>;
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-12">
                    <button className="btn btn-success btn-icon-split" onClick={async (e) => this.resetModal()} data-toggle="modal" data-target="#userModal">
                    <span className="icon text-white-50">
                        <FontAwesomeIcon icon={faPlus}/>
                    </span>
                        <span className="text">Ajouter un utilisateur</span>
                    </button>
                </div>
            </div>


            <UserModal
                id={this.state.user.id}
                firstname={this.state.user.firstname}
                lastname={this.state.user.lastname}
                email={this.state.user.email}
                password={this.state.user.password}
                role={this.state.user.role}
            />

        </div>;
    }
}
