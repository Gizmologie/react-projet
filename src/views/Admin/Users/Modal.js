import React, {Component} from 'react';
import UserService from '../../../services/user.service';

export default class Modal extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            user: {
                id: props.id,
                firstname: props.firstname,
                lastname: props.lastname,
                email: props.email,
                role: props.role
            },
            restricted: props.restricted !== null ? props.restricted : false,
            editedField: [],
            roles: [
                {
                    label: 'Utilisateur',
                    value: 0
                },
                {
                    label: 'Administrateur',
                    value: 10
                }
            ]
        };
    }

    async componentWillReceiveProps(nextProps) {
        await this.setState({
            user: {
                id: nextProps.id,
                firstname: nextProps.firstname,
                lastname: nextProps.lastname,
                email: nextProps.email,
                role: nextProps.role
            },
            restricted: nextProps.restricted !== null
                ? nextProps.restricted
                : false

        });
    }

    async formSubmit(e) {
        e.preventDefault();
        let data = new FormData();
        let value = null;
        if (this.state.user.id) {
            for (let [key] of Object.entries(this.state.editedField)) {
                value = this.state.user[key];
                value = Array.isArray(value) ? value.map((el) => {
                    return el._id;
                }) : value;
                data.append(key, value);
            }
            if (this.state.restricted !== null && this.state.restricted) {
                await UserService.accountUpdate(this.state.user.id, data);
            } else {
                await UserService.update(this.state.user.id, data);
            }
        } else {
            let key = null;
            Object.entries(this.state.user).forEach((el) => {
                value = el[1];
                key = el[0];
                value = Array.isArray(value) ? value.map((el) => {
                    return el._id;
                }) : value;
                data.append(key, value);
            });
            await UserService.store(data);
        }

        document.dispatchEvent(new Event('userModalClose'));
        this.setState({
            editedField: []
        });
        let close = document.querySelector('#userModalClose');
        if (close) {
            close.click();
        }

    }

    async handleChange(e, type = 'default') {

        let value = null;

        switch (type) {
            case 'file':
                value = e.target.files[0];
                break;
            default:
            case 'default':
                value = e.target.value;
                break;
        }

        this.setState({
            user: {
                ...this.state.user,
                [e.target.name]: value
            }
        });

        if (this.state.user.id) {
            this.setState({
                editedField: {
                    ...this.state.editedField,
                    [e.target.name]: true
                }
            });

        }
    }

    isRoleOfUser(role, user) {
        return parseInt(user.role) === parseInt(role.value);
    }

    render() {
        return <div className="modal fade" id="userModal" tabIndex="-1" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Utilisateur</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" id={'userModalClose'}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <form id={'addGenderForm'} onSubmit={async (event) => await this.formSubmit(
                            event)}>
                            <div className="form-group">
                                <label>Prénom</label>
                                <input required="required" type="text" className="form-control" id="firstname" name={'firstname'} value={this.state.user.firstname} onChange={(e) => this.handleChange(
                                    e)}/>
                            </div>
                            <div className="form-group">
                                <label>Nom</label>
                                <input required="required" type="text" className="form-control" id="lastname" name={'lastname'} value={this.state.user.lastname} onChange={(e) => this.handleChange(
                                    e)}/>
                            </div>
                            <div className="form-group">
                                <label>E-mail</label>
                                <input required="required" type="email" className="form-control" id="email" name={'email'} value={this.state.user.email} onChange={(e) => this.handleChange(
                                    e)}/>
                            </div>
                            <div className="form-group">
                                <label>Photo de profil</label>
                                <input type="file" className="form-control" id="thumbnail" name={'thumbnail'} onChange={(e) => this.handleChange(
                                    e, 'file')}/>
                            </div>
                            {
                                this.state.user.id ? '' :
                                    <div className="form-group">
                                        <label>Mot de passe</label>
                                        <input required={this.state.user.id
                                            ? 'required'
                                            : ''} type="password" className="form-control" id="password" name={'password'} value={this.state.user.password} onChange={(e) => this.handleChange(
                                            e)}/>
                                    </div>
                            }
                            {
                                !this.state.restricted ?
                                    <div className="form-group">
                                        <label>Role</label>
                                        <select required="required" className="form-control" id="role" name={'role'} onChange={(e) => this.handleChange(
                                            e, 'select')}>
                                            <option value="" disabled selected>Choisir
                                                le rôle
                                            </option>
                                            {this.state.roles.map((role, i) => {
                                                return <option selected={this.isRoleOfUser(
                                                    role, this.state.user)
                                                    ? 'selected'
                                                    : ''} value={role.value}>{role.label}</option>;
                                            })}
                                        </select>
                                    </div> : ''
                            }

                            <input type="hidden" className="form-control" id="_id" name={'id'} value={this.state.user.id}/>
                            <button type="submit" className="btn btn-primary">{this.state.user.id
                                ? 'Modifier'
                                : 'Ajouter'}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>;
    }
}

