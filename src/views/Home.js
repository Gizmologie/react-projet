import React, {Component} from 'react';
import PostsService from '../services/post.service';
import moment from 'moment';
export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            post: [],
            posts: []
        };
    }

    async componentDidMount() {
        try {
            let posts = await PostsService.last();
            let post = posts[0];
            this.setState({
                post,
                posts
            });
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        let {post} = this.state;
        let {posts} = this.state;

        return <div className="container pt-5">

            <div className="jumbotron p-3 p-md-5 text-white rounded bg-dark">
                <div className="col-md-6 px-0">
                    <h2 className="display-4 font-italic">{post.title}</h2>
                    <p className="lead my-3">{post.content}</p>
                    <p className="lead mb-0">
                        <a href={`/article/${post._id}`} className="text-white font-weight-bold">Continuer
                            de lire ...
                        </a>
                    </p>
                </div>
            </div>

            <div className="row" >
                {
                    posts.map((post, index) => {
                        return <div key={index} className="col-md-6">
                            <div className="card flex-md-row mb-4 box-shadow h-md-250">
                                <div className="card-body d-flex flex-column align-items-start">
                                    <strong className={`d-inline-block mb-2 ${this.getTextColor(index)}`} >{post.category.label}</strong>
                                    <h3 className="mb-0">
                                        <span className="text-dark" >{post.title}</span>
                                    </h3>
                                    <div className="mb-1 text-muted">{moment(post.created_at).format('d MMM Y')}</div>
                                    <p className="card-text mb-auto">{post.content}</p>
                                    <a href={`/article/${post._id}`}>Voir plus</a>
                                </div>

                            </div>
                        </div>
                    })
                }
            </div>
        </div>;

    }

    getTextColor(index) {
        switch (index){
            default:
            case 0:
                return 'text-primary';
            case 1:
                return 'text-success';
            case 2:
                return 'text-danger';
            case 3:
                return 'text-warning';
        }
    }
}
